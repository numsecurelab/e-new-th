package io.space.ethereumkit.api.storage

import android.arch.persistence.room.*
import io.space.ethereumkit.api.models.LastBlockHeight

@Dao
interface LastBlockHeightDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(lastBlockHeight: LastBlockHeight)

    @Query("SELECT * FROM LastBlockHeight")
    fun getLastBlockHeight(): LastBlockHeight?

    @Delete
    fun delete(lastBlockHeight: LastBlockHeight)

    @Query("DELETE FROM LastBlockHeight")
    fun deleteAll()
}
