package io.space.ethereumkit.spv.net.tasks

import io.space.ethereumkit.spv.core.ITask
import io.space.ethereumkit.spv.models.BlockHeader

class BlockHeadersTask(val blockHeader: BlockHeader, val limit: Int, val reverse: Boolean = false) : ITask