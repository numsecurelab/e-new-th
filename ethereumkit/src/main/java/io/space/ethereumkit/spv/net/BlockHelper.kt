package io.space.ethereumkit.spv.net

import io.space.ethereumkit.core.ISpvStorage
import io.space.ethereumkit.network.INetwork
import io.space.ethereumkit.spv.models.BlockHeader

class BlockHelper(val storage: ISpvStorage, val network: INetwork) {

    val lastBlockHeader: BlockHeader
        get() = storage.getLastBlockHeader() ?: network.checkpointBlock

}
