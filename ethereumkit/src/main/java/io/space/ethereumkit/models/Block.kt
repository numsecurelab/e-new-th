package io.space.ethereumkit.models

data class Block(val number: Long, val timestamp: Long)
